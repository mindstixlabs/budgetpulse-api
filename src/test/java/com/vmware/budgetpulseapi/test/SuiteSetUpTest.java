/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

/**
 * Setting up the environment.
 * 
 * @author Mindstix
 * 
 */
public class SuiteSetUpTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(SuiteSetUpTest.class);
  public static Properties vmwareEnvProps = new Properties();

  /**
   * Setting the environment properties before executing the test cases.
   */
  @BeforeSuite
  public static void setUp() throws Exception {
    String propertyPath = "src" + File.separator + "test" + File.separator + "resources"
        + File.separator;
    String env = System.getProperty("env.config");
    String port = System.getProperty("env.port");
    String dockerIp = System.getProperty("dockerip");
    try {
      LOGGER.info("Loading {} property file.", env);
      InputStream input = new FileInputStream(propertyPath + env + ".properties");
      vmwareEnvProps.load(input);
      IOUtils.closeQuietly(input);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured ", e);
    }

    // Call the methods from the BearerToken to get the access token
    // BearerToken bearertoken = new BearerToken();
    // bearertoken.setup(Constants.DOCKER_IP , port);
    // bearertoken.selectionOfDomain();
    // bearertoken.loginValidCred();
    // bearertoken.getBearerToken();
    // bearertoken.tearDown();
  }
}
