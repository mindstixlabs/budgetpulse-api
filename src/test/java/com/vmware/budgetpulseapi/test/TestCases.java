/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */

package com.vmware.budgetpulseapi.test;

import com.aventstack.extentreports.Status;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.jayway.restassured.response.Response;
import com.vmware.budgetpulseapi.ResponseHandler;
import com.vmware.budgetpulseapi.SchemaValidation;
import com.vmware.budgetpulseapi.utils.BaseApi;
import com.vmware.budgetpulseapi.utils.Constants;
import com.vmware.budgetpulseapi.utils.ExtentManager;
import com.vmware.budgetpulseapi.utils.ExtentTestCase;
import com.vmware.budgetpulseapi.utils.RestAssuredUtility;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Execute all test cases.
 * 
 * @author Mindstix
 */
// @Listeners(CustomReporter.class)
public class TestCases {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestCases.class);
  private Response response;
  private Long responseTime;
  private int responseStatusCode;
  private BaseApi baseapi = new BaseApi();
  private SchemaValidation schemaValidation = new SchemaValidation();
  private ResponseHandler responseHandler = new ResponseHandler();

  /**
   * set the Base URI and Path.
   * 
   */
  @BeforeClass(groups = { "regression", "smoke" })
  public void setup() {
    LOGGER.info("Setup baseUrl and basePath for BudgetPulseApi api");
    RestAssuredUtility.setBaseUri(SuiteSetUpTest.vmwareEnvProps.getProperty("baseURL"));
    RestAssuredUtility.setBasePath(SuiteSetUpTest.vmwareEnvProps.getProperty("basePath"));
    // ExtentTestCase.setExtent(ExtentManager.getExtent());
    ExtentTestCase.EXTENT = ExtentManager.getExtent();
  }

  /**
   * Contains a endpoint url list of budgetPulse api.
   * 
   */
  @DataProvider(name = "budgetPulseApiList")
  public Object[][] budgetPulseApiList() throws InterruptedException {
    LOGGER.info("In API DataProvider ");
    return Constants.apiName;
  }

  /**
   * Contains a wrong endpoint url list of people search api.
   * 
   */
  @DataProvider(name = "falseEndpointUrl")
  public Object[][] trueApiName() throws InterruptedException {
    LOGGER.info("In API DataProvider");
    return Constants.falseApiName;
  }

  /**
   * Contains a endpoint url list of people search api.
   * 
   */
  @DataProvider(name = "peopleSearchApiList")
  public Object[][] peopleSearchApiList() throws InterruptedException {
    LOGGER.info("In API DataProvider ");
    return Constants.peopleSearchApiName;
  }

  /**
   * To verify 200 status code and response time.
   */
  @Test(groups = { "regression",
      "smoke" }, dataProvider = "budgetPulseApiList", priority = 1, enabled = true)
  public void verifyStatusCodesAndResTime(String apiName, String extendedUrl)
      throws JsonParseException, JsonMappingException, IOException {
    LOGGER.info("Executing 1st test case.");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 200 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = baseapi.getResponseForAllTheApi(true, Constants.API_METHOD_POST, apiName,
        extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    LOGGER.info("Response Time of : {} : {} ms ", apiName, responseTime);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_200, "Status code should be 200.");
    LOGGER.info("Executed 1st test case.");
  }

  /**
   * To verify schema validation for all APIs.
   */
  @Test(groups = {
      "regression" }, dataProvider = "budgetPulseApiList", priority = 2, enabled = true)
  public void verifySchemaValidation(String apiName, String extendedUrl) {
    LOGGER.info("Executing 2nd test case.");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify schema validation for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = schemaValidation.jsonSchemaValidationForRestApi(apiName, extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    LOGGER.info("Response Time of : {} : {} ms ", apiName, responseTime);
    LOGGER.info("Executed 2nd test case.");
  }

  // /**
  // * To verify status code 400 (Bad Request).
  // */
  // @Test(groups = { "regression" }, dataProvider = "budgetPulseApiList",
  // priority = 3, enabled = true)
  // public void verifyBadRequestStatus(String apiName, String extendedUrl) {
  // LOGGER.info("-----------------------------started------------------------------");
  // LOGGER.info("Executing 3rd test case ");
  // ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest("Verify 400 status
  // code for API :" + apiName,
  // "Endpoint Url is : " + extendedUrl);
  // response = responseHandler.getResponseForAllTheApi(true,
  // Constants.BAD_REQUEST, apiName, extendedUrl);
  // responseStatusCode = responseHandler.getStatusCode(response, apiName,
  // extendedUrl);
  // ExtentTestCase.getTest().info(
  // "Actual Status Code:" + responseStatusCode + " | Expected Status Code:" +
  // Constants.STATUSCODE_400);
  // Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_400, "Status
  // code should be 400.");
  // LOGGER.info("Executed 3rd test case.");
  // LOGGER.info("-----------------------------ended------------------------------");
  // }

  /**
   * To verify 404 (Page Not Found) status code.
   */
  @Test(groups = { "regression" }, dataProvider = "falseEndpointUrl", priority = 4, enabled = true)
  public void verifyPageNotFoundStatusCodes(String apiName, String extendedUrl)
      throws JsonParseException, JsonMappingException, IOException {
    LOGGER.info(" Executing 4th test case ");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 404 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = responseHandler.getResponseForAllTheApi(true, Constants.API_METHOD_POST, apiName,
        extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    ExtentTestCase.getTest().info("Actual Status Code:" + responseStatusCode
        + " | Expected Status Code:" + Constants.STATUSCODE_404);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_404, "Status code should be 404.");
    LOGGER.info("Executed 4th test case.");
  }

  /**
   * To verify 405 (Method Not Allowed).
   */
  @Test(groups = {
      "regression" }, dataProvider = "budgetPulseApiList", priority = 5, enabled = true)
  public void verifyMethodNotAllowedStatusCode(String apiName, String extendedUrl) {
    LOGGER.info(" Executing 5th test case ");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 405 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = responseHandler.getResponseForAllTheApi(true, Constants.METHOD_NOT_ALLOWED_POST,
        apiName, extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    responseHandler.verifyStatusCode(responseStatusCode, Constants.STATUSCODE_405);
    LOGGER.info(" Executed 5th test case ");
  }

  /**
   * To check 401 (Unauthorized) status code.
   */
  @Test(groups = {
      "regression" }, dataProvider = "budgetPulseApiList", priority = 6, enabled = true)
  public void verifyUnauthorizedStatusCodes(String apiName, String extendedUrl) {
    LOGGER.info(" Executing 6th test case ");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 401 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = responseHandler.getResponseForAllTheApi(true, Constants.UNAUTH_POST, apiName,
        extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    ExtentTestCase.getTest().info("Actual Status Code:" + responseStatusCode
        + " | Expected Status Code:" + Constants.STATUSCODE_401);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_401, "Status code should be 401.");
    LOGGER.info(" Executed 6th test case ");
  }

  /**
   * To verify 406 Status Code (Not acceptable).
   */
  @Test(groups = {
      "regression" }, dataProvider = "budgetPulseApiList", priority = 7, enabled = true)
  public void verifyNotAcceptableStatusCode(String apiName, String extendedUrl) {
    LOGGER.info(" Executing 7th test case ");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 406 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    response = responseHandler.getResponseForAllTheApi(true, Constants.NOT_ACCEPT_POST, apiName,
        extendedUrl);
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    responseHandler.verifyStatusCode(responseStatusCode, Constants.STATUSCODE_405);
    LOGGER.info(" Executed 7th test case ");
  }

  /**
   * To verify 200 status code and response time.
   */
  @Test(groups = { "regression", "smoke" }, priority = 8, enabled = true)
  public void getPeoplesearchAccessToken()
      throws JsonParseException, JsonMappingException, IOException {
    LOGGER.info("Executing 8th test case.");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT
        .createTest("Getting peopleSearch accessToken with Endpoint Url is : "
            + Constants.PEOPLESEARCH_ACCESS_TOKEN_ENDURL);
    response = baseapi.getPeopleSearchAccessToken(Constants.PEOPLESEARCH_ACCESS_TOKEN_ENDURL);
    responseStatusCode = responseHandler.getStatusCode(response, "PeoplesearchAccessToken",
        Constants.PEOPLESEARCH_ACCESS_TOKEN_ENDURL);
    responseTime = responseHandler.getResponseTime(response, "PeoplesearchAccessToken");
    LOGGER.info("Response Time of : {} : {} ms ", "PeoplesearchAccessToken", responseTime);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_200, "Status code should be 200.");
    LOGGER.info("Executed 1st test case.");
  }

  /**
   * To verify 200 status code and response time.
   */
  @Test(groups = { "regression",
      "smoke" }, dataProvider = "peopleSearchApiList", priority = 9, enabled = true)
  public void verifyStatusCodesAndResTimeForPeoplesearch(String apiName, String extendedUrl)
      throws JsonParseException, JsonMappingException, IOException {
    LOGGER.info("Executing 9th test case.");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify 200 status code for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    if (apiName.contains(Constants.API_METHOD_POST)) {
      response = responseHandler.getResponseForAllTheApi(false, Constants.API_METHOD_POST, apiName,
          extendedUrl);
    } else {
      response = responseHandler.getResponseForAllTheApi(false, Constants.API_METHOD_GET, apiName,
          extendedUrl);
    }
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    LOGGER.info("Response Time of : {} : {} ms ", apiName, responseTime);
    Assert.assertEquals(responseStatusCode, Constants.STATUSCODE_200, "Status code should be 200.");
    LOGGER.info("Executed 1st test case.");
  }

  /**
   * To verify schema validation f or all APIs.
   */
  @Test(groups = {
      "regression" }, dataProvider = "peopleSearchApiList", priority = 10, enabled = true)
  public void verifySchemaValidationForPeoplesearch(String apiName, String extendedUrl) {
    LOGGER.info("Executing 10th test case.");
    ExtentTestCase.TEST = ExtentTestCase.EXTENT.createTest(
        "Verify schema validation for API :" + apiName, "Endpoint Url is : " + extendedUrl);
    if (apiName.contains(Constants.API_METHOD_POST)) {
      response = schemaValidation.jsonSchemaValidationForPeoplesearchPostApi(apiName, extendedUrl);
    } else {
      response = schemaValidation.jsonSchemaValidationForPeoplesearchGetApi(apiName, extendedUrl);
    }
    responseStatusCode = responseHandler.getStatusCode(response, apiName, extendedUrl);
    responseTime = responseHandler.getResponseTime(response, apiName);
    LOGGER.info("Response Time of : {} : {} ms ", apiName, responseTime);
    LOGGER.info("Executed 10th test case.");
  }

  /**
   * AfterTest Method to get the Test Result.
   */
  @AfterMethod(groups = { "regression", "smoke" })
  public void getResult(ITestResult result) {
    LOGGER.info("Recording Results");
    if (result.getStatus() == ITestResult.FAILURE) {
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getName());
      ExtentTestCase.getTest().fail(Status.FAIL + ", Failed TestCase is " + result.getThrowable());
    } else if (result.getStatus() == ITestResult.SKIP) {
      ExtentTestCase.getTest().skip(Status.SKIP + ", Skipped TestCase is " + result.getName());
    } else {
      ExtentTestCase.getTest().pass(Status.PASS + " , Passed TestCase is : " + result.getName());
    }
    ExtentTestCase.getExtent().flush();
  }

  /**
   * Reset base URI and Base path.
   */
  @AfterClass(groups = { "regression", "smoke" })
  public void setDown() {
    RestAssuredUtility.resetBaseUri();
    RestAssuredUtility.resetBasePath();
  }

}
