/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

/**
 * Setting params and headers.
 * 
 * @author Mindstix
 *
 */
public final class HeadersParamsConstants {

  // public static final String SCOPE = "read";
  public static final String AUTHORIZATION_ALEART = "Basic U19CVURHRVRfQVBQOldlbGNvbWUuMw==";
  public static final String AUTHORIZATION_PEOPLE_SEARCH = "Basic cGVvcGxlZmluZGVyOmFkZGQ2N2QwZGUwNDQ0N2RiZTBmOTI5ZDA2NTE5YjBl";
  public static final String CONTENT_TYPE = "application/json";
  public static final String ACCEPT = "application/json";
  public static final String WRONG_ACCEPT_VALUE = "AAA";
  public static final String APPNAMEVALUE = "budgetPulseApi";
  public static final String AUTHORIZATION = "Authorization";
  public static final String AUTHORIZATION_BEARER = "Bearer ";
  public static final String STR_TENANT_ID = "tenant_id";
  public static final String TENANT_ID = "tn-63abc8b9";

}
