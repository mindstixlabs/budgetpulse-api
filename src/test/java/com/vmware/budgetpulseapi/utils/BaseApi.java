/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

import static com.jayway.restassured.RestAssured.given;

import com.aventstack.extentreports.Status;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.SSLConfig;
import com.jayway.restassured.response.Response;
import com.vmware.budgetpulseapi.test.SuiteSetUpTest;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

/**
 * Contains response, response status code , response time.
 * 
 * @author Mindstix
 *
 */
public class BaseApi {
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseApi.class);
  public static String peopleSearchAccessToken;
  private Response response;
  private Long responseTime;

  public enum HttpMethodName {
    GET, POST, unauthorizedPost, methodNotAllowedPost, notAcceptableGet, badRequest, notAcceptablePost;
  }

  /**
   * This method is used to get response for all the APIs.
   */
  public Response getResponseForAllTheApi(boolean isbudgetPulse, String webApiMethodName,
      String apiName, String extendedUrl) {
    LOGGER.info("File Path: {}", apiName);
    LOGGER.info("File Path: {}", extendedUrl);
    LOGGER.info("Method name: {}", webApiMethodName);
    HttpMethodName httpMethodName = HttpMethodName.valueOf(webApiMethodName);
    switch (httpMethodName) {
      case GET:
        if (isbudgetPulse) {
          LOGGER.info("No GET method written for budgetPulse yet");
        } else {
          LOGGER.info("In GET request call");
          response = getUserDetails(apiName, extendedUrl);
        }

        break;
      case POST:
        LOGGER.info("In post request call");
        if (isbudgetPulse) {
          response = getResponseOfPostRequestCall(apiName, extendedUrl);
        } else {
          response = peopleSearchPostRequestCall(apiName, extendedUrl);
        }
        break;
      case methodNotAllowedPost:
        LOGGER.info("Method not allowed api : Post request call");
        response = getMethodNotAllowedResponsePost(apiName, extendedUrl);
        break;
      case badRequest:
        LOGGER.info(" Bad request type");
        response = postBadRequestResponse(apiName, extendedUrl);
        break;
      case unauthorizedPost:
        LOGGER.info("Post call for unauthorized api");
        response = getUnauthorizedResponseForPostRequest(apiName, extendedUrl);
        break;
      case notAcceptablePost:
        LOGGER.info("Post call for not acceptable api");
        response = getNotAcceptableResponsePost(apiName, extendedUrl);
        break;
      default:
        LOGGER.error("Invalid Request Type");
    }
    return response;
  }

  /**
   * This method is used to get method not allowed response when passing wrong
   * request get call.
   */
  private Response getMethodNotAllowedResponsePost(String apiName, String endpointUrl) {
    response = given()
        .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
        .all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_ALEART)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when().get(endpointUrl)
        .then().extract().response();
    ExtentTestCase.getTest().info("API Response = " + response.body().prettyPrint());
    LOGGER.info("Response of  : {} ", response.body().prettyPrint());
    ExtentTestCase.getTest().log(Status.INFO, apiName + " API hitted successfully");
    return response;
  }

  /**
   * This method is used to get response when required header parameter is not
   * passing and get bad request call.
   * 
   * @return : response : Bad request type.
   */
  private Response postBadRequestResponse(String apiName, String endPointUrl) {
    if (apiName.contains(Constants.API_METHOD_POST)) {
      LOGGER.info("Post request calling : Bad Request");
      response = given()
          .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
          .all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_ALEART)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).when()
          .post(endPointUrl).then().extract().response();
    }
    ExtentTestCase.getTest().info("API Response = " + response.body().prettyPrint());
    LOGGER.info("Response of  : {} ", response.body().prettyPrint());
    ExtentTestCase.getTest().log(Status.INFO, apiName + " API hitted successfully");
    return response;
  }

  /**
   * This method is used to get not acceptable response for post request call.
   * 
   * @apiName : Name of the api.
   * @endpointUrl : endpoint url of the api.
   * @return : response
   */

  private Response getNotAcceptableResponsePost(String apiName, String endpointUrl) {
    LOGGER.info("Post Call for : {} ", apiName);
    String apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    response = given()
        .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
        .all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_ALEART)
        .header(Constants.ACCEPT, HeadersParamsConstants.WRONG_ACCEPT_VALUE).body(payload).when()
        .post(endpointUrl).then().extract().response();
    LOGGER.info("Status Code: {}", response.statusCode());
    ExtentTestCase.getTest().info("API Response = " + response.body().prettyPrint());
    LOGGER.info("Response of  : {} ", response.body().prettyPrint());
    ExtentTestCase.getTest().log(Status.INFO, apiName + " API hitted successfully");
    return response;
  }

  /**
   * This method is used to get response for post call without passing bearer
   * token.
   * 
   * @param unauthorizedApiName
   *          : Name of the Api.
   * @param unauthorizedEndpointUrl
   *          :Endpoint Url of the Api.
   * @return : response
   */
  private Response getUnauthorizedResponseForPostRequest(String unauthorizedApiName,
      String unauthorizedEndpointUrl) {
    LOGGER.info("Post Call for : {} ", unauthorizedApiName);
    String apiNameJson = unauthorizedApiName.substring(unauthorizedApiName.lastIndexOf("_") + 1);
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    response = given()
        .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
        .all().header(Constants.AUTHORIZATION, "")
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).body(payload).when()
        .post(unauthorizedEndpointUrl).then().extract().response();
    LOGGER.info("Status Code: {}", response.statusCode());
    ExtentTestCase.getTest().log(Status.INFO, unauthorizedApiName + " API hitted successfully");
    return response;
  }

  /**
   * This method is used to get response for post call.
   * 
   * @apiName : Name of the api .
   * @endpointUrl : Endpoint url of the api.
   * @return : response
   */
  private Response getResponseOfPostRequestCall(String apiName, String endpointUrl) {
    LOGGER.info("Post Call for : {} ", apiName);
    String apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    response = given()
        .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
        .all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_ALEART)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).body(payload).when()
        .post(endpointUrl).then().extract().response();
    LOGGER.info("Status Code: {}", response.statusCode());
    ExtentTestCase.getTest().log(Status.INFO, apiName + " API hitted successfully");
    return response;
  }

  /**
   * get PeopleSearch AccessToken.
   */
  public Response getPeopleSearchAccessToken(String endpointUrl) {
    RestAssuredUtility
        .setBaseUri(SuiteSetUpTest.vmwareEnvProps.getProperty("peopleSearchAuthBaseUrl"));
    RestAssuredUtility
        .setBasePath(SuiteSetUpTest.vmwareEnvProps.getProperty("peopleSearchAuthbasePath"));
    Response response = given()
        .header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_PEOPLE_SEARCH).when()
        .post(endpointUrl).then().extract().response();
    JSONObject jsonResponse = new JSONObject(response.asString());
    peopleSearchAccessToken = jsonResponse.getString("access_token");
    return response;
  }

  /**
   * Gets user details.
   */
  public Response getUserDetails(String apiName, String endpointUrl) {

    if (!peopleSearchAccessToken.equals(null)) {
      RestAssuredUtility
          .setBaseUri(SuiteSetUpTest.vmwareEnvProps.getProperty("peopleSearchBaseUrl"));
      RestAssuredUtility
          .setBasePath(SuiteSetUpTest.vmwareEnvProps.getProperty("peopleSearchbasePath"));
      response = given().log().path()
          .header(HeadersParamsConstants.AUTHORIZATION,
              HeadersParamsConstants.AUTHORIZATION_BEARER + peopleSearchAccessToken)
          .header(HeadersParamsConstants.STR_TENANT_ID, HeadersParamsConstants.TENANT_ID).when()
          .get(endpointUrl).then().extract().response();
    }
    return response;
  }

  /**
   * This method is used to get response for post call.
   * 
   * @apiName : Name of the api .
   * @endpointUrl : Endpoint url of the api.
   * @return : response
   */
  private Response peopleSearchPostRequestCall(String apiName, String endpointUrl) {
    LOGGER.info("Post Call for : {} ", apiName);
    String apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    JsonObject jsonObj = null;
    try {
      jsonObj = (JsonObject) json
          .parse(new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_BEARER + peopleSearchAccessToken)
        .header(HeadersParamsConstants.STR_TENANT_ID, HeadersParamsConstants.TENANT_ID)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE).body(payload).when()
        .post(endpointUrl).then().extract().response();
    LOGGER.info("Status Code: {}", response.statusCode());
    ExtentTestCase.getTest().log(Status.INFO, apiName + " API hitted successfully");
    return response;
  }

  /**
   * This method is used to get status code for all the APIs.
   */
  public int getStatusCode(Response response, String apiName, String endpointUrl) {
    String responseStatusCode = Integer.toString(response.statusCode());
    LOGGER.info("Calling : {} Api ", apiName);
    LOGGER.info("Status Code : {} ", responseStatusCode);
    if (response.statusCode() == 200) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      ExtentTestCase.getTest()
          .info("API Response - Data Extracted from " + apiName + ": API successfully");
    } else if (response.statusCode() == 400) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Invalid status value : {}", apiName);
      ExtentTestCase.getTest()
          .info("API Response of API - " + apiName + " =: Invalid status value");
    } else if (response.statusCode() == 401) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Authentication Failed : {} ", apiName);
      ExtentTestCase.getTest()
          .info("API Response of API - " + apiName + " =: Authentication Failed");
    } else if (response.statusCode() == 403) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Permission Unavailable : {} ", apiName);
      ExtentTestCase.getTest()
          .info("API Response of API - " + apiName + " =: Permission Unavailable");
    } else if (response.statusCode() == 404) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Page Not Found : {} ", apiName);
      ExtentTestCase.getTest().info("Response of " + apiName + " API =: Page Not Found");
    } else if (response.statusCode() == 405) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Method not Allowed : {} ", apiName);
      ExtentTestCase.getTest().info("API Response of API - " + apiName + " =: Method not Allowed");
    } else if (response.statusCode() == 406) {
      ExtentTestCase.getTest().info("Api Response status code : " + responseStatusCode);
      LOGGER.info("API Response = Not Acceptable : {} ", apiName);
      ExtentTestCase.getTest().info("API Response of - " + apiName + " =: Not Acceptable ");
    } else if (response.statusCode() == 500) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = Internal Server Error : {}", apiName);
      ExtentTestCase.getTest()
          .info("API Response of API - " + apiName + " =: Internal Server Error");
    } else if (response.statusCode() == 0) {
      ExtentTestCase.getTest().info("API Response Status Code = " + responseStatusCode);
      LOGGER.info("API Response = No Response : {} ", apiName);
      ExtentTestCase.getTest().info("API Response of API - " + apiName + " =: No Response");
    }
    return response.statusCode();
  }

  /**
   * This method is used to get response time for all the APIs and validate
   * response time.
   */
  public Long getResponseTime(Response response, String apiName) {
    responseTime = response.time();
    // if (responseTime <= Constants.EXP_RESP_TIME4 && responseTime >
    // Constants.EXP_RESP_TIME3) {
    // ExtentTestCase.getTest().warning("API Response Time = " + responseTime +
    // "ms");
    // LOGGER.warn("Api Response time is lie between 4s and 3s : {}", apiName);
    // } else
    if (responseTime <= Constants.EXP_RESP_TIME3) {
      ExtentTestCase.getTest().info("API Response Time = " + responseTime + "ms");
      LOGGER.info("Api Resonse time is valid and its less than or equal to 3s : {}", apiName);
    } else {
      ExtentTestCase.getTest().fail("API Response Time = " + responseTime + "ms, Its more than 300 ms");
      LOGGER.error(
          "Api Resonse time is more than 300ms and its invalid : {} Need response time optimization",
          apiName);
    }
    return responseTime;
  }

  public void verifyStatusCode(int responseStatusCode, int statuscode405) {
    if (responseStatusCode != statuscode405) {
      ExtentTestCase.getTest().warning(
          "Actual Status Code:" + responseStatusCode + " | Expected Status Code:" + statuscode405);
      LOGGER.warn(
          "Actual Status Code:" + responseStatusCode + " | Expected Status Code:" + statuscode405);
    } else {
      Assert.assertEquals(responseStatusCode, statuscode405);
    }

  }

}