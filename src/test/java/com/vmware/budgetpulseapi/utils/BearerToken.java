/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

import com.vmware.budgetpulseapi.test.SuiteSetUpTest;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Login and get the Authentication code.
 * 
 * @author Mindstix
 * 
 */

public class BearerToken {
  // Declaring all the private and public members
  private static final Logger LOGGER = LoggerFactory.getLogger(BearerToken.class);
  private WebDriver driver;
  public static String bearerToken;
  private String loginUrl = SuiteSetUpTest.vmwareEnvProps.getProperty("loginUrl");

  /**
   * Set up for to launch Chrome browser and set the Base URI and Path.
   * 
   */

  public void setup() {
    LOGGER.info("Login Url : {} ", loginUrl);
    ChromeDriverManager.getInstance().setup();
    ChromeOptions co = new ChromeOptions();
    co.addArguments("--headless");
    driver = new ChromeDriver(co);
    // driver = new ChromeDriver();
    driver.get(loginUrl);

  }

  /**
   * 
   * @param dockerIp.
   * @param portNum.
   */
  public void setup(String dockerIp, String portNum) throws MalformedURLException {
    ChromeDriverManager.getInstance().setup();
    ChromeOptions co = new ChromeOptions();
    co.addArguments("--headless");
    co.addArguments("test-type");
    co.addArguments("disable-infobars");
    DesiredCapabilities cap = DesiredCapabilities.chrome();
    cap.setCapability(ChromeOptions.CAPABILITY, co);
    int port = Integer.parseInt((String) portNum);
    if (port == 0) {
      driver = new ChromeDriver(co);
    } else {
      URL url = new URL("http://" + dockerIp + ":" + port + "/wd/hub");
      LOGGER.info("Setting remote driver url as : {}", url);
      driver = new RemoteWebDriver(url, cap);
    }
    driver.get(loginUrl);

  }

  /**
   * Selection of domain from the dropdown list : vmware.
   *
   */
  public void selectionOfDomain() {
    WebDriverWait wait = new WebDriverWait(driver, 90);
    LOGGER.info("Select the domain");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constants.DROPDOWN_ID)));
    driver.findElement(By.id(Constants.DROPDOWN_ID)).sendKeys(Constants.DROPDOWN_VALUE);
    driver.findElement(By.id(Constants.NEXT_ID)).click();
  }

  // /**
  // * Login to the application.
  // *
  // */
  // public void loginValidCred() {
  // // Enter user name
  // WebDriverWait wait = new WebDriverWait(driver, 90);
  // LOGGER.info("Entering name");
  // wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
  // driver.findElement(By.id("username")).clear();
  // driver.findElement(By.id("username")).sendKeys(RestAssuredUtility
  // .decode(Constants.USER_NAME));
  // LOGGER.info("Entered user name : {} ", Constants.USER_NAME);
  // // Enter Password
  // driver.findElement(By.id("password")).clear();
  // driver.findElement(By.id("password"))
  // .sendKeys(RestAssuredUtility.decode(Constants.USER_PASSWORD));
  // LOGGER.info("Entered password : {} ", Constants.USER_PASSWORD);
  // // Click on sign in button
  // driver.findElement(By.id("signIn")).click();
  // LOGGER.info("Clicked on sign in button");
  // // Wait till page load and click on grant button
  // String currentUrl = driver.getCurrentUrl();
  // LOGGER.info("Current URL: {} ", currentUrl);
  // if ((currentUrl.contains(Constants.REDIRECT_URI))) {
  // WebElement grantButton =
  // driver.findElement(By.xpath("//*[@id='authorize']"));
  // wait.until(ExpectedConditions.elementToBeClickable(grantButton));
  // grantButton.click();
  // }

  /**
   * Set bearer token.
   */
  public static void setBearerToken(String cookieValue) {
    bearerToken = cookieValue;
  }

  /**
   * Get the Bearer token.
   */
  public void getBearerToken() {
    // loop for getting the cookie information
    for (Cookie ck : driver.manage().getCookies()) {
      if (Constants.HZN_COOKIE.equals(ck.getName())) {
        setBearerToken(ck.getValue());
        LOGGER.info("Bearer Token : {} ", bearerToken);
      }
    }
  }

  /**
   * Quit driver.
   */
  public void tearDown() {
    LOGGER.info("tearDown :");
    driver.close();
  }

}
