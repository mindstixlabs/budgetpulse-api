/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

/**
 * Constants.
 * 
 * @author Mindstix.
 *
 */
public final class Constants {
  public static final String DOCKER_IP = "10.193.2.123";
  public static final String AUTHORIZATION = "Authorization";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final String WRONG_CONTENT_TYPE = "aaaa";
  public static final String REDIRECT_URI = "redirect_uri";
  public static final String ACCEPT = "Accept";
  public static final String USER = "user";
  public static final String DROPDOWN_ID = "userStoreDomain";
  public static final String DROPDOWN_VALUE = "System Domain";
  public static final String NEXT_ID = "userStoreFormSubmit";
  public static final String APPNAME = "appName";
  public static final String HZN_COOKIE = "HZN";
  public static final String API_METHOD_GET = "GET";
  public static final String API_METHOD_POST = "POST";
  public static final String API_METHOD_PUT = "PUT";
  public static final String UNAUTH_GET = "unauthorizedGet";
  public static final String NOT_ACCEPT_GET = "notAcceptableGet";
  public static final String NEW_ACCESS_TOKEN = "newAccessToken";
  public static final String METHOD_NOT_ALLOWED = "methodNotAllowed";
  public static final String METHOD_NOT_ALLOWED_POST = "methodNotAllowedPost";
  public static final String BAD_REQUEST = "badRequest";
  public static final String UNAUTH_POST = "unauthorizedPost";
  public static final String NOT_ACCEPT_POST = "notAcceptablePost";
  public static final String FILTER = "filter";
  public static final int EXP_RESP_TIME3 = 300;
  public static final int EXP_RESP_TIME4 = 4000;
  public static final int EXP_RESP_TIME5 = 5000;
  public static final int EXP_RESP_TIME10 = 10000;
  public static final int STATUSCODE_400 = 400;
  public static final int STATUSCODE_500 = 500;
  public static final int STATUSCODE_401 = 401;
  public static final int STATUSCODE_404 = 404;
  public static final int STATUSCODE_405 = 405;
  public static final int STATUSCODE_406 = 406;
  public static final int STATUSCODE_200 = 200;
 

  public static final String[][] apiName = {
      { "POST_CostCenterList", "/getCostcenterQuarterlyUtilization.xsjs" },
      { "POST_Last5Transactions", "/getLast5Transaction.xsjs" },
      { "POST_BudgetUtilisation", "/getProjectUtilizationQuarterly.xsjs?PROJECT=103810" },
      { "POST_OverviewGraph", "/getTotalProjectUtilization.xsjs" },
      { "POST_CostcentersPerProject", "/getCostcenterQuarterlyUtilization.xsjs?PROJECT=103808" },
      { "POST_BudgetBreakup", "/getProjectBreakupQuarterly.xsjs?PROJECT=103810" },
      { "POST_RemainingDaysInCurrentQuarter", "/getCurrentQuarterlyDays.xsjs" },
      { "POST_ProjectStatusOverview", "/getProjectStatus.xsjs" },
      { "POST_CostCenterProjectsList",
          "/getCostcenterWiseProjectLast5Transaction.xsjs?COSTCENTER=IN1079701" },
      { "POST_VendorList", "/getVendorQuarterlyUtilization.xsjs" },
      { "POST_CostcenterWiseVendor5Transactions",
          "/getCostcenterWiseVendorLast5Transaction.xsjs?COSTCENTER=US1019666" },
      { "POST_VendorsPerProject", "/getVendorQuarterlyUtilization.xsjs?PROJECT=103810" },
      { "POST_CostcenterWiseProject5Transactions",
          "/getCostcenterWiseProjectLast5Transaction.xsjs?COSTCENTER=US1019666" },
      { "POST_ProjectsList", "/getProjectUtilization.xsjs" },
      { "POST_TotalProjectsUtilisation", "/getTotalProjectUtilizationExecutive.xsjs" },
      { "POST_GetTotalCountPerProject", "/getCountPMProjectVendorExecutive.xsjs" },
      { "POST_BudgetUtilisationForProjectManager", "/getPMUtilizationQuarterly.xsjs?PMID=326901" },
      { "POST_BudgetUtilisationForAllProjectManagers", "/getPMWiseCurrentQtrUtilization.xsjs" },
      { "POST_ListOfAllProjectsUnderADirector", "/getAllProjectsUnderDirectorsMgr.xsjs" },
      { "POST_CostCentersListExec", "/getCostcenterQuarterlyUtilizationExecutive.xsjs" },
      { "POST_GetEmpId", "/getEmployeeId.xsjs" } };

  public static final String[][] falseApiName = {
      { "POST_CostCenterList", "/getCostcenterQuarterlyUtilizatn.xsjs" },
      { "POST_Last5Transactions", "/getLast5Transactin.xsjs" },
      { "POST_BudgetUtilisation", "/getProjectUtilizationQuartey.xsjs?PROJE=103810" },
      { "POST_OverviewGraph", "/getTotalProjectUtilizion.xsjs" },
      { "POST_CostcentersPerProject", "/getCostcenterQuarterlyUtilizatn.xsjs?PROJECT=103808" },
      { "POST_BudgetBreakup", "/getProjectBreakupQuarrly.xsjs?PRCT=103810" },
      { "POST_RemainingDaysInCurrentQuarter", "/getCurrentQuartlyDays.xsjs" },
      { "POST_ProjectStatusOverview", "/getProjectStus.xsjs" },
      { "POST_CostCenterProjectsList",
          "/getCostcenterWiseProjectLast5Transfsaction.xsjs?COSTCENTER=IN1079701" },
      { "POST_VendorList", "/getVendorQuarterlyUtilizsion.xsjs" },
      { "POST_CostcenterWiseVendor5Transactions",
          "/getCostcenterWiseVeorLast5Transaction.xsjs?STCENTER=US1019666" },
      { "POST_VendorsPerProject", "/getVendorQuarterlyUtilizatn.xsjs?PROJECT=103810" },
      { "POST_CostcenterWiseProject5Transactions",
          "/getCostcenterWiseojectLast5Transaction.xsjs?COSTCENTER=US1019666" },
      { "POST_ProjectsList", "/getProjectUtilizaon.xsjs" },
      { "POST_TotalProjectsUtilisation", "/getTotalProjectUtilizationExecuve.xsjs" },
      { "POST_GetTotalCountPerProject", "/getCountPMProjectVendorExecive.xsjs" },
      { "POST_BudgetUtilisationForProjectManager", "/getPMUtilizationQuarrly.xsjs?PMID=326901" },
      { "POST_BudgetUtilisationForAllProjectManagers", "/getPMWiseCurrentQtrilization.xsjs" },
      { "POST_ListOfAllProjectsUnderADirector", "/getAllProjectsUnderDirectsMgr.xsjs" },
      { "POST_CostCentersListExec", "/getCostcenterQuarterlyUtilizationExutive.xsjs" },
      { "POST_GetEmpId", "/getEmpyeeId.xsjs" } };
  
  
  public static final String PEOPLESEARCH_ACCESS_TOKEN_ENDURL = "/token?grant_type=client_credentials&redirect_uri=https%3A%2F%2Fmbei.vmware.com%3A8443%2Foauth-server%2Faccess_response";
  public static final String[][] peopleSearchApiName = {
	      { "GET_UserDetails", "/users/v1/search/query?q=adevanathan@vmware.com" },
	      { "POST_ManagerHierarachy","/users/v1/hierarchy/managers"},
	      { "POST_DirectReports","/users/v1/direct/reports"}
	      };
  
  

}
