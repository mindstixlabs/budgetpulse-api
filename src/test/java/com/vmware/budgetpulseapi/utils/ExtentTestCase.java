/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

/**
 * Contains objects of extent report.
 * 
 * @author Mindstix
 */
public class ExtentTestCase {
  public static ExtentTest TEST;
  public static ExtentReports EXTENT;
  
  public static ExtentTest getTest() {
    return TEST;
  }

  public static void setTest(ExtentTest test) {
    TEST = test;
  }

  public static ExtentReports getExtent() {
    return EXTENT;
  }

  public static void setExtent(ExtentReports extent) {
    EXTENT = extent;
  }

}
