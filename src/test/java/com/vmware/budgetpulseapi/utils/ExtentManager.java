/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extent report configurations.
 * 
 * @author Mindstix
 *
 */
public class ExtentManager {
  private static final Logger LOGGER = LoggerFactory.getLogger(ExtentManager.class);
  private static ExtentReports extent;
  private static ExtentTest test;
  private static ExtentHtmlReporter htmlReporter;
  private static String filePath = "./extentreport.html";

  /**
   * Creation of instance for HTML file.
   */
  public static synchronized ExtentReports getExtent() {
    if (extent != null) {
      return extent; // avoid creating new instance of html file
    }
    extent = new ExtentReports();
    extent.attachReporter(getHtmlReporter());
    return extent;
  }

  /**
   * Creation of instance for HTML Reporter.
   */

  private static ExtentHtmlReporter getHtmlReporter() {
    htmlReporter = new ExtentHtmlReporter(filePath);

    // make the charts visible on report open
    htmlReporter.config().setChartVisibilityOnOpen(true);

    htmlReporter.config().setDocumentTitle("VMware BudgetPulse API Automation Report");
    String suiteName = System.getProperty("suiteName");
    LOGGER.info("Suite Name : {}", suiteName);
    htmlReporter.config().setReportName("peoplesearch API  : " + suiteName + " Cycle");
    return htmlReporter;
  }

  /**
   * Creation of Test Case.
   */
  public static ExtentTest createTest(String name, String description) {
    test = extent.createTest(name, description);
    return test;
  }
}
