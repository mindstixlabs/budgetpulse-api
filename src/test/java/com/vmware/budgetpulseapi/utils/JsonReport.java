/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Generates a custom json format report.
 * 
 */
public class JsonReport implements IReporter {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonReport.class);

  @SuppressWarnings({ "unchecked" })
  public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
      String outputDirectory) {
    JSONArray results = new JSONArray();
    suites.forEach(element -> {
      JSONObject resultObj = createSuiteJsonObject(element);
      if (!resultObj.isEmpty()) {
        results.add(resultObj);
      }
    });
    try (FileWriter file = new FileWriter(outputDirectory + File.separator + "report.json")) {
      if (results.size() == 1) {
        file.write(StringEscapeUtils.unescapeJava(results.get(0).toString()));
      } else {
        file.write(StringEscapeUtils.unescapeJava(results.toJSONString()));
      }
    } catch (IOException e) {
      LOGGER.error("Error while creating the json report.", e);
    }
  }

  @SuppressWarnings({ "unchecked" })
  public JSONObject createSuiteJsonObject(ISuite suite) {
    JSONObject result = new JSONObject();
    JSONArray passedMethods = new JSONArray();
    JSONArray failedMethods = new JSONArray();
    JSONArray skippedMethods = new JSONArray();
    JSONArray AllMethods = new JSONArray();
    JSONObject suiteData = new JSONObject();
    Map<String, ISuiteResult> suiteResults = suite.getResults();
    try {
      System.out.println(new ObjectMapper().writeValueAsString(suiteResults));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    suite.getResults().entrySet().forEach(element -> {
      ITestContext context = element.getValue().getTestContext();
      passedMethods.addAll(createResultJsonArray(context.getPassedTests().getAllResults()));
      failedMethods.addAll(createResultJsonArray(context.getFailedTests().getAllResults()));
      skippedMethods.addAll(createResultJsonArray(context.getSkippedTests().getAllResults()));
      AllMethods.addAll(passedMethods);
      AllMethods.addAll(failedMethods);
      AllMethods.addAll(skippedMethods);
      suiteData.put("suiteName", context.getName());
      suiteData.put("startTime", context.getStartDate().getTime());
      suiteData.put("endTime", context.getEndDate().getTime());
    });
    result.put("Passed", passedMethods.size());
    result.put("Failed", failedMethods.size());
    result.put("Skipped", skippedMethods.size());   
    result.put("groupEndTime", StringEscapeUtils.unescapeJava(formatDate((long) suiteData.get("endTime"))));
    result.put("groupStartTime", StringEscapeUtils.unescapeJava(formatDate((long) suiteData.get("startTime"))));
    result.put("groupData", AllMethods);
    result.put("Executed", AllMethods.size());
    result.put("groupDuration",
        getExecutionTime((long) suiteData.get("startTime"), (long) suiteData.get("endTime")));
    result.put("groupInfo", suiteData.get("suiteName").toString());
    return result;
  }

  public String formatDate(long time) {
    Date date = new Date(time);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    return sdf.format(date); 
  }

  public String getExecutionTime(long start, long end) {
    long second = 1000l;
    long minute = 60l * second;
    long hour = 60l * minute;
    Date startDate = new Date(start);
    Date endDate = new Date(end);
    long duration = endDate.getTime() - startDate.getTime();
    String time = String.format("%02d", duration / hour) + ":"
        + String.format("%02d", (duration % hour) / minute) + ":"
        + String.format("%02d", (duration % minute) / second);
    return time;
  }

  @SuppressWarnings("unchecked")
  public JSONArray createResultJsonArray(Set<ITestResult> results) {
    JSONArray result = new JSONArray();
    results.forEach(element -> {
      JSONObject currentJsonResult = new JSONObject();
      currentJsonResult.put("testScriptName", element.getName());
      currentJsonResult.put("scriptStartTime", StringEscapeUtils.unescapeJava(formatDate(element.getStartMillis())));
      currentJsonResult.put("scriptEndTime", StringEscapeUtils.unescapeJava(formatDate(element.getEndMillis())));
      currentJsonResult.put("scriptDuration",
          getExecutionTime(element.getStartMillis(), element.getEndMillis()));
      if (element.getStatus() == 1) {
        currentJsonResult.put("status", "passed");
      } else if (element.getStatus() == 2) {
        currentJsonResult.put("status", "failed");
      } else {
        currentJsonResult.put("status", "skipped");
      }
      result.add(currentJsonResult);
    });
    return result;
  }

}
