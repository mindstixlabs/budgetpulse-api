/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.budgetpulseapi;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.SSLConfig;
import com.jayway.restassured.response.Response;
import com.vmware.budgetpulseapi.test.SuiteSetUpTest;
import com.vmware.budgetpulseapi.utils.BaseApi;
import com.vmware.budgetpulseapi.utils.Constants;
import com.vmware.budgetpulseapi.utils.ExtentTestCase;
import com.vmware.budgetpulseapi.utils.HeadersParamsConstants;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JSON Schema validation.
 * 
 * @author Mindstix.
 *
 */

public class SchemaValidation {
  private static final Logger LOGGER = LoggerFactory.getLogger(SchemaValidation.class);
  private Response response;
  private String apiNameJson;
  private String schemaFileName;
  private JsonObject jsonObj = null;

  /**
   * Schema validation for post Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   */
  public Response jsonSchemaValidationForRestApi(String apiName, String endpointUrl) {

    apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    schemaFileName = "jsonFiles/" + apiNameJson + ".json";
    
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    try {
      jsonObj = (JsonObject) json.parse(
          new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: \n {} ", payload);
    LOGGER.info("File Path : {} ", apiName);
    LOGGER.info("File Path: {} ", endpointUrl);
    LOGGER.info("Schema File Name: {} ", schemaFileName);
    LOGGER.info(" In Post method ");
    if (!schemaFileName.isEmpty()) {
      response = given()
          .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
          .all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION_ALEART)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).body(payload).when()
          .post(endpointUrl).then()
          .statusCode(200).and().body(matchesJsonSchemaInClasspath(schemaFileName)).extract()
          .response();
      LOGGER.info("Response: {}", response.body().prettyPrint());
      ExtentTestCase.getTest().pass("JSON Schema matched");
      LOGGER.info("JSON Schema matched ");
      return response;
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.info("Schema File Not Present");
      return response;
    }
  }
  
  /**
   * Schema validation for peoplesearch post Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   */
  public Response jsonSchemaValidationForPeoplesearchPostApi(String apiName, String endpointUrl) {

    apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    schemaFileName = "jsonFiles/" + apiNameJson + ".json";
    
    LOGGER.info("apiNameJson : {}", apiNameJson);
    JsonParser json = new JsonParser();
    try {
      jsonObj = (JsonObject) json.parse(
          new FileReader(SuiteSetUpTest.vmwareEnvProps.getProperty(apiNameJson)));
    } catch (JsonIOException e) {
      LOGGER.error("Error occured. ", e);
    } catch (JsonSyntaxException e) {
      LOGGER.error("Error occured. ", e);
    } catch (FileNotFoundException e) {
      LOGGER.error("Error occured. ", e);
    }
    String payload = null;
    if (jsonObj != null) {
      payload = jsonObj.toString();
    }
    LOGGER.info("Payload: {} ", payload);
    LOGGER.info("API name : {} ", apiName);
    LOGGER.info("End Url: {} ", endpointUrl);
    LOGGER.info("Schema File Name: {} ", schemaFileName);
    LOGGER.info(" In Post method ");
    if (!schemaFileName.isEmpty()) {
      response = given()
          .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
          .all().header(Constants.AUTHORIZATION,HeadersParamsConstants.AUTHORIZATION_BEARER + BaseApi.peopleSearchAccessToken)
          .header(HeadersParamsConstants.STR_TENANT_ID, HeadersParamsConstants.TENANT_ID)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).body(payload).when()
          .post(endpointUrl).then()
          .statusCode(200).and().body(matchesJsonSchemaInClasspath(schemaFileName)).extract()
          .response();
      LOGGER.info("Response: {}", response.body().prettyPrint());
      ExtentTestCase.getTest().pass("JSON Schema matched");
      LOGGER.info("JSON Schema matched ");
      return response;
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.info("Schema File Not Present");
      return response;
    }
  }
  
  
  /**
   * Schema validation for peoplesearch Get Api call.
   * 
   * @param apiName
   *          : name of the api.
   * @param endpointUrl
   *          : endpoint url of the api.
   */
  public Response jsonSchemaValidationForPeoplesearchGetApi(String apiName, String endpointUrl) {

    apiNameJson = apiName.substring(apiName.lastIndexOf("_") + 1);
    schemaFileName = "jsonFiles/" + apiNameJson + ".json";
    
    LOGGER.info("apiNameJson : {}", apiNameJson);
    LOGGER.info("API Name : {} ", apiName);
    LOGGER.info("End Url: {} ", endpointUrl);
    LOGGER.info("Schema File Name: {} ", schemaFileName);
    LOGGER.info(" In Post method ");
    if (!schemaFileName.isEmpty()) {
      response = given()
          .config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation())).log()
          .all().header(Constants.AUTHORIZATION,HeadersParamsConstants.AUTHORIZATION_BEARER + BaseApi.peopleSearchAccessToken)
          .header(HeadersParamsConstants.STR_TENANT_ID, HeadersParamsConstants.TENANT_ID)
          .header(Constants.CONTENT_TYPE, HeadersParamsConstants.CONTENT_TYPE)
          .header(Constants.ACCEPT, HeadersParamsConstants.ACCEPT).when()
          .get(endpointUrl).then()
          .statusCode(200).and().body(matchesJsonSchemaInClasspath(schemaFileName)).extract()
          .response();
      LOGGER.info("Response: {}", response.body().prettyPrint());
      ExtentTestCase.getTest().pass("JSON Schema matched");
      LOGGER.info("JSON Schema matched ");
      return response;
    } else {
      ExtentTestCase.getTest().info("Schema File Not Present");
      LOGGER.info("Schema File Not Present");
      return response;
    }
  }

}
