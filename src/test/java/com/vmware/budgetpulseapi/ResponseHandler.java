/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 */

package com.vmware.budgetpulseapi;

import com.jayway.restassured.response.Response;
import com.vmware.budgetpulseapi.utils.BaseApi;

/**
 * Contains response status code, response, response time and make Request body
 * and get data from pojo class.
 * 
 * @author Mindstix.
 *
 */
public class ResponseHandler extends BaseApi {
  private Response response;
  public int responseStatusCode;

  /**
   * Generate response , response status code, response time .
   * 
   * @param httpMethodName
   *          : Request type.
   * @param apiName
   *          : name of the api.
   * @param extendedUrl
   *          : endpoint url of the api.
   */
  public void generateResponseStatusCodeAndTime(Boolean isBudgetPulse,String httpMethodName, String apiName,
      String extendedUrl) {
    response = getResponseForAllTheApi(isBudgetPulse, httpMethodName, apiName, extendedUrl);
    responseStatusCode = getStatusCode(response, apiName, extendedUrl);
    getResponseTime(response, apiName);
  }
}
